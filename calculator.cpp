#include "calculator.h"
#include "ui_calculator.h"
#include "mathbuttontype.h"
#include "add_button.h"
#include "divide_button.h"
#include "multiply_button.h"
#include "substract_button.h"

double calcVal = 0.0;

double first_val = 0.0;
double second_val = 0.0;

Divide_Button div_button("/",nullptr);
Multiply_Button mult_button("*",nullptr);
Add_Button add_button("+",nullptr);
Substract_Button sub_button("-",nullptr);


QVector<MathButtonType *> MathButtons =
{
    &div_button,
    &mult_button,
    &add_button,
    &sub_button,
};

QVector<QPushButton *> pushButtons(SUM_OF_TRIGGERS, nullptr);

Calculator::Calculator(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Calculator)
{
    ui->setupUi(this);

    ui->Display->setText(QString::number(calcVal));
    QPushButton *numButtons[10];

    for(int i = 0; i < 10; ++i)
    {
        QString butName = "Button" + QString::number(i);
        numButtons[i] = Calculator::findChild<QPushButton *>(butName);
        connect(numButtons[i], SIGNAL(released()), this, SLOT(NumPressed()));
    }

    pushButtons[_DIV_TRIGGER] = ui->Divide;
    pushButtons[_MULT_TRIGGER] = ui->Multiply;
    pushButtons[_ADD_TRIGGER] = ui->Add;
    pushButtons[_SUB_TRIGGER] = ui->Subtract;

    for(int i = 0; i < MathButtons.size(); ++i)
    {
        MathButtons[i]->set_Push_Button(pushButtons[i]);
        connect(MathButtons[i]->my_push_button_ptr, SIGNAL(released()), this, SLOT(MathButtonPressed()));
    }

    connect(ui->Equals, SIGNAL(released()), this, SLOT(EqualButton()));
    connect(ui->ChangeSign, SIGNAL(released()), this, SLOT(ChangeNumberSign()));
    connect(ui->Clear, SIGNAL(released()), this, SLOT(Clear()));

}

Calculator::~Calculator()
{
    delete ui;
}

void Calculator::NumPressed()
{
    QPushButton * button = (QPushButton *)sender();
    QString butVal = button->text();
    QString displayVal = ui->Display->text();

    if(displayVal.toDouble() == 0 || displayVal.toDouble() == 0.0)
    {
        ui->Display->setText(butVal);
    }
    else
    {
        QString newVal = displayVal + butVal;
        double dblNewVal = newVal.toDouble();
        butVal = QString::number(dblNewVal, 'g', 16);
        ui->Display->setText(butVal);
    }

}

void Calculator::MathButtonPressed()
{
    for(int i = 0; i < MathButtons.size(); ++i)
    {
        MathButtons[i]->set_triggerflag(false);
    }

    QString dispVal = ui->Display->text();
    if(first_val != 0.0)
    {
        second_val = dispVal.toDouble();
    }
    else
    {
        first_val = dispVal.toDouble();
    }


    QPushButton *button = (QPushButton *)sender();
    QString butVal = button->text();

    for(int i = 0; i < MathButtons.size(); ++i)
    {
        if(QString::compare(butVal, MathButtons[i]->get_math_char(), Qt::CaseSensitive) == 0)
        {
            MathButtons[i]->set_triggerflag(true);
            break;
        }
    }

    if(second_val != 0.0)
    {
        EqualButton();
    }
    else
    {
        ui->Display->setText("");
    }
}

void Calculator::EqualButton()
{
    QString dispVal = ui->Display->text();
    second_val = dispVal.toDouble();

    for(int i = 0; i < MathButtons.size(); ++i)
    {
        if(MathButtons[i]->get_triggerflag())
        {
            calcVal = MathButtons[i]->calculate(first_val, second_val);
        }
    }

    ui->Display->setText(QString::number(calcVal));
    first_val = 0.0;
    second_val = 0.0;
}

void Calculator::ChangeNumberSign()
{
    QRegExp reg("[-]?[0-9.]*");

    if(reg.exactMatch(ui->Display->text()))
    {
        double dispVal = ui->Display->text().toDouble();
        dispVal *= -1;
        ui->Display->setText(QString::number(dispVal));
    }
}

void Calculator::Clear()
{
  ui->Display->setText("");
  first_val = 0.0;
  second_val = 0.0;
}

