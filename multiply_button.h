#ifndef MULTIPLY_BUTTON_H
#define MULTIPLY_BUTTON_H

#include "mathbuttontype.h"

class Multiply_Button : public MathButtonType
{
public:
    Multiply_Button(QString math_char, QPushButton * push_button_ptr);
    double calculate(double first_val, double second_val);
};

#endif // MULTIPLY_BUTTON_H
