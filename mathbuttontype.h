#ifndef MATHBUTTONTYPE_H
#define MATHBUTTONTYPE_H

#include <QString>
#include <QPushButton>

class MathButtonType
{
public:
    MathButtonType(QString math_char, QPushButton * push_button_ptr);
    bool triggerflag;
    virtual double calculate(double first_val, double second_val) = 0;
    void set_Push_Button(QPushButton * push_button_ptr);
    void set_triggerflag(bool flag);
    bool get_triggerflag();
    QString get_math_char();
    QString my_math_char;
    QPushButton * my_push_button_ptr;
};

#endif // MATHBUTTONTYPE_H
