#include "mathbuttontype.h"

MathButtonType::MathButtonType(QString math_char, QPushButton * push_button_ptr)
{
    triggerflag = false;
    my_math_char = math_char;
    my_push_button_ptr = push_button_ptr;
}

void MathButtonType::set_Push_Button(QPushButton * push_button_ptr)
{
    my_push_button_ptr = push_button_ptr;
}

void MathButtonType::set_triggerflag(bool flag)
{
    triggerflag = flag;
}

bool MathButtonType::get_triggerflag()
{
    return triggerflag;
}

QString MathButtonType::get_math_char()
{
    return my_math_char;
}

