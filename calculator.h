#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Calculator; }
QT_END_NAMESPACE

enum
{
    _DIV_TRIGGER = 0,
    _MULT_TRIGGER,
    _ADD_TRIGGER,
    _SUB_TRIGGER,

    SUM_OF_TRIGGERS
};

class Calculator : public QMainWindow
{
    Q_OBJECT

public:
    Calculator(QWidget *parent = nullptr);
    ~Calculator();

private:
    Ui::Calculator *ui;

private slots:
    void NumPressed();
    void MathButtonPressed();
    void EqualButton();
    void ChangeNumberSign();
    void Clear();
};
#endif // CALCULATOR_H
