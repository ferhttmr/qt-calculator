#ifndef DIVIDE_BUTTON_H
#define DIVIDE_BUTTON_H

#include "mathbuttontype.h"

class Divide_Button : public MathButtonType
{
public:
    Divide_Button(QString math_char, QPushButton * push_button_ptr);
    double calculate(double first_val, double second_val);
};

#endif // DIVIDE_BUTTON_H
