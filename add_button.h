#ifndef ADD_BUTTON_H
#define ADD_BUTTON_H

#include "mathbuttontype.h"

class Add_Button : public MathButtonType
{
public:
    Add_Button(QString math_char, QPushButton * push_button_ptr);
    double calculate(double first_val, double second_val);
};

#endif // ADD_BUTTON_H
