#ifndef SUBSTRACT_BUTTON_H
#define SUBSTRACT_BUTTON_H

#include "mathbuttontype.h"

class Substract_Button : public MathButtonType
{
public:
    Substract_Button(QString math_char, QPushButton * push_button_ptr);
    double calculate(double first_val, double second_val);
};

#endif // SUBSTRACT_BUTTON_H
