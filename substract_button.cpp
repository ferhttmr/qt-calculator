#include "substract_button.h"

Substract_Button::Substract_Button(QString math_char, QPushButton * push_button_ptr) : MathButtonType(math_char, push_button_ptr)
{

}

double Substract_Button::calculate(double first_val, double second_val)
{
    return first_val - second_val;
}
